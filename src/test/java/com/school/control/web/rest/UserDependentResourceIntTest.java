package com.school.control.web.rest;

import com.school.control.SchoolcontrolbackendApp;

import com.school.control.domain.UserDependent;
import com.school.control.repository.UserDependentRepository;
import com.school.control.service.UserDependentService;
import com.school.control.service.dto.UserDependentDTO;
import com.school.control.service.mapper.UserDependentMapper;
import com.school.control.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.school.control.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserDependentResource REST controller.
 *
 * @see UserDependentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SchoolcontrolbackendApp.class)
public class UserDependentResourceIntTest {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CI = "AAAAAAAAAA";
    private static final String UPDATED_CI = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVATED = false;
    private static final Boolean UPDATED_ACTIVATED = true;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private UserDependentRepository userDependentRepository;

    @Autowired
    private UserDependentMapper userDependentMapper;

    @Autowired
    private UserDependentService userDependentService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserDependentMockMvc;

    private UserDependent userDependent;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserDependentResource userDependentResource = new UserDependentResource(userDependentService);
        this.restUserDependentMockMvc = MockMvcBuilders.standaloneSetup(userDependentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserDependent createEntity(EntityManager em) {
        UserDependent userDependent = new UserDependent()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .ci(DEFAULT_CI)
            .activated(DEFAULT_ACTIVATED)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        return userDependent;
    }

    @Before
    public void initTest() {
        userDependent = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserDependent() throws Exception {
        int databaseSizeBeforeCreate = userDependentRepository.findAll().size();

        // Create the UserDependent
        UserDependentDTO userDependentDTO = userDependentMapper.toDto(userDependent);
        restUserDependentMockMvc.perform(post("/api/user-dependents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDependentDTO)))
            .andExpect(status().isCreated());

        // Validate the UserDependent in the database
        List<UserDependent> userDependentList = userDependentRepository.findAll();
        assertThat(userDependentList).hasSize(databaseSizeBeforeCreate + 1);
        UserDependent testUserDependent = userDependentList.get(userDependentList.size() - 1);
        assertThat(testUserDependent.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testUserDependent.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testUserDependent.getCi()).isEqualTo(DEFAULT_CI);
        assertThat(testUserDependent.isActivated()).isEqualTo(DEFAULT_ACTIVATED);
        assertThat(testUserDependent.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUserDependent.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testUserDependent.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testUserDependent.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createUserDependentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userDependentRepository.findAll().size();

        // Create the UserDependent with an existing ID
        userDependent.setId(1L);
        UserDependentDTO userDependentDTO = userDependentMapper.toDto(userDependent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserDependentMockMvc.perform(post("/api/user-dependents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDependentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserDependent in the database
        List<UserDependent> userDependentList = userDependentRepository.findAll();
        assertThat(userDependentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserDependents() throws Exception {
        // Initialize the database
        userDependentRepository.saveAndFlush(userDependent);

        // Get all the userDependentList
        restUserDependentMockMvc.perform(get("/api/user-dependents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userDependent.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].ci").value(hasItem(DEFAULT_CI.toString())))
            .andExpect(jsonPath("$.[*].activated").value(hasItem(DEFAULT_ACTIVATED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getUserDependent() throws Exception {
        // Initialize the database
        userDependentRepository.saveAndFlush(userDependent);

        // Get the userDependent
        restUserDependentMockMvc.perform(get("/api/user-dependents/{id}", userDependent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userDependent.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.ci").value(DEFAULT_CI.toString()))
            .andExpect(jsonPath("$.activated").value(DEFAULT_ACTIVATED.booleanValue()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserDependent() throws Exception {
        // Get the userDependent
        restUserDependentMockMvc.perform(get("/api/user-dependents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserDependent() throws Exception {
        // Initialize the database
        userDependentRepository.saveAndFlush(userDependent);

        int databaseSizeBeforeUpdate = userDependentRepository.findAll().size();

        // Update the userDependent
        UserDependent updatedUserDependent = userDependentRepository.findById(userDependent.getId()).get();
        // Disconnect from session so that the updates on updatedUserDependent are not directly saved in db
        em.detach(updatedUserDependent);
        updatedUserDependent
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .ci(UPDATED_CI)
            .activated(UPDATED_ACTIVATED)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        UserDependentDTO userDependentDTO = userDependentMapper.toDto(updatedUserDependent);

        restUserDependentMockMvc.perform(put("/api/user-dependents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDependentDTO)))
            .andExpect(status().isOk());

        // Validate the UserDependent in the database
        List<UserDependent> userDependentList = userDependentRepository.findAll();
        assertThat(userDependentList).hasSize(databaseSizeBeforeUpdate);
        UserDependent testUserDependent = userDependentList.get(userDependentList.size() - 1);
        assertThat(testUserDependent.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testUserDependent.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testUserDependent.getCi()).isEqualTo(UPDATED_CI);
        assertThat(testUserDependent.isActivated()).isEqualTo(UPDATED_ACTIVATED);
        assertThat(testUserDependent.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserDependent.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserDependent.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testUserDependent.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingUserDependent() throws Exception {
        int databaseSizeBeforeUpdate = userDependentRepository.findAll().size();

        // Create the UserDependent
        UserDependentDTO userDependentDTO = userDependentMapper.toDto(userDependent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserDependentMockMvc.perform(put("/api/user-dependents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDependentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserDependent in the database
        List<UserDependent> userDependentList = userDependentRepository.findAll();
        assertThat(userDependentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserDependent() throws Exception {
        // Initialize the database
        userDependentRepository.saveAndFlush(userDependent);

        int databaseSizeBeforeDelete = userDependentRepository.findAll().size();

        // Delete the userDependent
        restUserDependentMockMvc.perform(delete("/api/user-dependents/{id}", userDependent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserDependent> userDependentList = userDependentRepository.findAll();
        assertThat(userDependentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserDependent.class);
        UserDependent userDependent1 = new UserDependent();
        userDependent1.setId(1L);
        UserDependent userDependent2 = new UserDependent();
        userDependent2.setId(userDependent1.getId());
        assertThat(userDependent1).isEqualTo(userDependent2);
        userDependent2.setId(2L);
        assertThat(userDependent1).isNotEqualTo(userDependent2);
        userDependent1.setId(null);
        assertThat(userDependent1).isNotEqualTo(userDependent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserDependentDTO.class);
        UserDependentDTO userDependentDTO1 = new UserDependentDTO();
        userDependentDTO1.setId(1L);
        UserDependentDTO userDependentDTO2 = new UserDependentDTO();
        assertThat(userDependentDTO1).isNotEqualTo(userDependentDTO2);
        userDependentDTO2.setId(userDependentDTO1.getId());
        assertThat(userDependentDTO1).isEqualTo(userDependentDTO2);
        userDependentDTO2.setId(2L);
        assertThat(userDependentDTO1).isNotEqualTo(userDependentDTO2);
        userDependentDTO1.setId(null);
        assertThat(userDependentDTO1).isNotEqualTo(userDependentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userDependentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userDependentMapper.fromId(null)).isNull();
    }
}
