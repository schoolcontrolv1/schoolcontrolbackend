package com.school.control.web.rest;

import com.school.control.SchoolcontrolbackendApp;

import com.school.control.domain.UserExtended;
import com.school.control.repository.UserExtendedRepository;
import com.school.control.service.UserExtendedService;
import com.school.control.service.dto.UserExtendedDTO;
import com.school.control.service.mapper.UserExtendedMapper;
import com.school.control.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.school.control.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserExtendedResource REST controller.
 *
 * @see UserExtendedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SchoolcontrolbackendApp.class)
public class UserExtendedResourceIntTest {

    private static final Integer DEFAULT_CI = 1;
    private static final Integer UPDATED_CI = 2;

    private static final LocalDate DEFAULT_BIRTH_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTH_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_BIRTH_PLACE = "AAAAAAAAAA";
    private static final String UPDATED_BIRTH_PLACE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final Integer DEFAULT_HOME_PHONE = 1;
    private static final Integer UPDATED_HOME_PHONE = 2;

    private static final Integer DEFAULT_CELL_PHONE = 1;
    private static final Integer UPDATED_CELL_PHONE = 2;

    private static final String DEFAULT_USER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_USER_CODE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_REGISTER_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REGISTER_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private UserExtendedRepository userExtendedRepository;

    @Autowired
    private UserExtendedMapper userExtendedMapper;

    @Autowired
    private UserExtendedService userExtendedService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserExtendedMockMvc;

    private UserExtended userExtended;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserExtendedResource userExtendedResource = new UserExtendedResource(userExtendedService);
        this.restUserExtendedMockMvc = MockMvcBuilders.standaloneSetup(userExtendedResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserExtended createEntity(EntityManager em) {
        UserExtended userExtended = new UserExtended()
            .ci(DEFAULT_CI)
            .birthDate(DEFAULT_BIRTH_DATE)
            .birthPlace(DEFAULT_BIRTH_PLACE)
            .country(DEFAULT_COUNTRY)
            .city(DEFAULT_CITY)
            .address(DEFAULT_ADDRESS)
            .homePhone(DEFAULT_HOME_PHONE)
            .cellPhone(DEFAULT_CELL_PHONE)
            .userCode(DEFAULT_USER_CODE)
            .registerDate(DEFAULT_REGISTER_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        return userExtended;
    }

    @Before
    public void initTest() {
        userExtended = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserExtended() throws Exception {
        int databaseSizeBeforeCreate = userExtendedRepository.findAll().size();

        // Create the UserExtended
        UserExtendedDTO userExtendedDTO = userExtendedMapper.toDto(userExtended);
        restUserExtendedMockMvc.perform(post("/api/user-extendeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtendedDTO)))
            .andExpect(status().isCreated());

        // Validate the UserExtended in the database
        List<UserExtended> userExtendedList = userExtendedRepository.findAll();
        assertThat(userExtendedList).hasSize(databaseSizeBeforeCreate + 1);
        UserExtended testUserExtended = userExtendedList.get(userExtendedList.size() - 1);
        assertThat(testUserExtended.getCi()).isEqualTo(DEFAULT_CI);
        assertThat(testUserExtended.getBirthDate()).isEqualTo(DEFAULT_BIRTH_DATE);
        assertThat(testUserExtended.getBirthPlace()).isEqualTo(DEFAULT_BIRTH_PLACE);
        assertThat(testUserExtended.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testUserExtended.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testUserExtended.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testUserExtended.getHomePhone()).isEqualTo(DEFAULT_HOME_PHONE);
        assertThat(testUserExtended.getCellPhone()).isEqualTo(DEFAULT_CELL_PHONE);
        assertThat(testUserExtended.getUserCode()).isEqualTo(DEFAULT_USER_CODE);
        assertThat(testUserExtended.getRegisterDate()).isEqualTo(DEFAULT_REGISTER_DATE);
        assertThat(testUserExtended.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUserExtended.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testUserExtended.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testUserExtended.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createUserExtendedWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userExtendedRepository.findAll().size();

        // Create the UserExtended with an existing ID
        userExtended.setId(1L);
        UserExtendedDTO userExtendedDTO = userExtendedMapper.toDto(userExtended);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserExtendedMockMvc.perform(post("/api/user-extendeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtendedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserExtended in the database
        List<UserExtended> userExtendedList = userExtendedRepository.findAll();
        assertThat(userExtendedList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserExtendeds() throws Exception {
        // Initialize the database
        userExtendedRepository.saveAndFlush(userExtended);

        // Get all the userExtendedList
        restUserExtendedMockMvc.perform(get("/api/user-extendeds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userExtended.getId().intValue())))
            .andExpect(jsonPath("$.[*].ci").value(hasItem(DEFAULT_CI)))
            .andExpect(jsonPath("$.[*].birthDate").value(hasItem(DEFAULT_BIRTH_DATE.toString())))
            .andExpect(jsonPath("$.[*].birthPlace").value(hasItem(DEFAULT_BIRTH_PLACE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].homePhone").value(hasItem(DEFAULT_HOME_PHONE)))
            .andExpect(jsonPath("$.[*].cellPhone").value(hasItem(DEFAULT_CELL_PHONE)))
            .andExpect(jsonPath("$.[*].userCode").value(hasItem(DEFAULT_USER_CODE.toString())))
            .andExpect(jsonPath("$.[*].registerDate").value(hasItem(DEFAULT_REGISTER_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getUserExtended() throws Exception {
        // Initialize the database
        userExtendedRepository.saveAndFlush(userExtended);

        // Get the userExtended
        restUserExtendedMockMvc.perform(get("/api/user-extendeds/{id}", userExtended.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userExtended.getId().intValue()))
            .andExpect(jsonPath("$.ci").value(DEFAULT_CI))
            .andExpect(jsonPath("$.birthDate").value(DEFAULT_BIRTH_DATE.toString()))
            .andExpect(jsonPath("$.birthPlace").value(DEFAULT_BIRTH_PLACE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.homePhone").value(DEFAULT_HOME_PHONE))
            .andExpect(jsonPath("$.cellPhone").value(DEFAULT_CELL_PHONE))
            .andExpect(jsonPath("$.userCode").value(DEFAULT_USER_CODE.toString()))
            .andExpect(jsonPath("$.registerDate").value(DEFAULT_REGISTER_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserExtended() throws Exception {
        // Get the userExtended
        restUserExtendedMockMvc.perform(get("/api/user-extendeds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserExtended() throws Exception {
        // Initialize the database
        userExtendedRepository.saveAndFlush(userExtended);

        int databaseSizeBeforeUpdate = userExtendedRepository.findAll().size();

        // Update the userExtended
        UserExtended updatedUserExtended = userExtendedRepository.findById(userExtended.getId()).get();
        // Disconnect from session so that the updates on updatedUserExtended are not directly saved in db
        em.detach(updatedUserExtended);
        updatedUserExtended
            .ci(UPDATED_CI)
            .birthDate(UPDATED_BIRTH_DATE)
            .birthPlace(UPDATED_BIRTH_PLACE)
            .country(UPDATED_COUNTRY)
            .city(UPDATED_CITY)
            .address(UPDATED_ADDRESS)
            .homePhone(UPDATED_HOME_PHONE)
            .cellPhone(UPDATED_CELL_PHONE)
            .userCode(UPDATED_USER_CODE)
            .registerDate(UPDATED_REGISTER_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        UserExtendedDTO userExtendedDTO = userExtendedMapper.toDto(updatedUserExtended);

        restUserExtendedMockMvc.perform(put("/api/user-extendeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtendedDTO)))
            .andExpect(status().isOk());

        // Validate the UserExtended in the database
        List<UserExtended> userExtendedList = userExtendedRepository.findAll();
        assertThat(userExtendedList).hasSize(databaseSizeBeforeUpdate);
        UserExtended testUserExtended = userExtendedList.get(userExtendedList.size() - 1);
        assertThat(testUserExtended.getCi()).isEqualTo(UPDATED_CI);
        assertThat(testUserExtended.getBirthDate()).isEqualTo(UPDATED_BIRTH_DATE);
        assertThat(testUserExtended.getBirthPlace()).isEqualTo(UPDATED_BIRTH_PLACE);
        assertThat(testUserExtended.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testUserExtended.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testUserExtended.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testUserExtended.getHomePhone()).isEqualTo(UPDATED_HOME_PHONE);
        assertThat(testUserExtended.getCellPhone()).isEqualTo(UPDATED_CELL_PHONE);
        assertThat(testUserExtended.getUserCode()).isEqualTo(UPDATED_USER_CODE);
        assertThat(testUserExtended.getRegisterDate()).isEqualTo(UPDATED_REGISTER_DATE);
        assertThat(testUserExtended.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserExtended.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserExtended.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testUserExtended.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingUserExtended() throws Exception {
        int databaseSizeBeforeUpdate = userExtendedRepository.findAll().size();

        // Create the UserExtended
        UserExtendedDTO userExtendedDTO = userExtendedMapper.toDto(userExtended);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserExtendedMockMvc.perform(put("/api/user-extendeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userExtendedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserExtended in the database
        List<UserExtended> userExtendedList = userExtendedRepository.findAll();
        assertThat(userExtendedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserExtended() throws Exception {
        // Initialize the database
        userExtendedRepository.saveAndFlush(userExtended);

        int databaseSizeBeforeDelete = userExtendedRepository.findAll().size();

        // Delete the userExtended
        restUserExtendedMockMvc.perform(delete("/api/user-extendeds/{id}", userExtended.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserExtended> userExtendedList = userExtendedRepository.findAll();
        assertThat(userExtendedList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserExtended.class);
        UserExtended userExtended1 = new UserExtended();
        userExtended1.setId(1L);
        UserExtended userExtended2 = new UserExtended();
        userExtended2.setId(userExtended1.getId());
        assertThat(userExtended1).isEqualTo(userExtended2);
        userExtended2.setId(2L);
        assertThat(userExtended1).isNotEqualTo(userExtended2);
        userExtended1.setId(null);
        assertThat(userExtended1).isNotEqualTo(userExtended2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserExtendedDTO.class);
        UserExtendedDTO userExtendedDTO1 = new UserExtendedDTO();
        userExtendedDTO1.setId(1L);
        UserExtendedDTO userExtendedDTO2 = new UserExtendedDTO();
        assertThat(userExtendedDTO1).isNotEqualTo(userExtendedDTO2);
        userExtendedDTO2.setId(userExtendedDTO1.getId());
        assertThat(userExtendedDTO1).isEqualTo(userExtendedDTO2);
        userExtendedDTO2.setId(2L);
        assertThat(userExtendedDTO1).isNotEqualTo(userExtendedDTO2);
        userExtendedDTO1.setId(null);
        assertThat(userExtendedDTO1).isNotEqualTo(userExtendedDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userExtendedMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userExtendedMapper.fromId(null)).isNull();
    }
}
