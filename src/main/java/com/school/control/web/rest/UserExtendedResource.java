package com.school.control.web.rest;
import com.school.control.service.UserExtendedService;
import com.school.control.web.rest.errors.BadRequestAlertException;
import com.school.control.web.rest.util.HeaderUtil;
import com.school.control.web.rest.util.PaginationUtil;
import com.school.control.service.dto.UserExtendedDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserExtended.
 */
@RestController
@RequestMapping("/api")
public class UserExtendedResource {

    private final Logger log = LoggerFactory.getLogger(UserExtendedResource.class);

    private static final String ENTITY_NAME = "userExtended";

    private final UserExtendedService userExtendedService;

    public UserExtendedResource(UserExtendedService userExtendedService) {
        this.userExtendedService = userExtendedService;
    }

    /**
     * POST  /user-extendeds : Create a new userExtended.
     *
     * @param userExtendedDTO the userExtendedDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userExtendedDTO, or with status 400 (Bad Request) if the userExtended has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-extendeds")
    public ResponseEntity<UserExtendedDTO> createUserExtended(@RequestBody UserExtendedDTO userExtendedDTO) throws URISyntaxException {
        log.debug("REST request to save UserExtended : {}", userExtendedDTO);
        if (userExtendedDTO.getId() != null) {
            throw new BadRequestAlertException("A new userExtended cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserExtendedDTO result = userExtendedService.save(userExtendedDTO);
        return ResponseEntity.created(new URI("/api/user-extendeds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-extendeds : Updates an existing userExtended.
     *
     * @param userExtendedDTO the userExtendedDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userExtendedDTO,
     * or with status 400 (Bad Request) if the userExtendedDTO is not valid,
     * or with status 500 (Internal Server Error) if the userExtendedDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-extendeds")
    public ResponseEntity<UserExtendedDTO> updateUserExtended(@RequestBody UserExtendedDTO userExtendedDTO) throws URISyntaxException {
        log.debug("REST request to update UserExtended : {}", userExtendedDTO);
        if (userExtendedDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserExtendedDTO result = userExtendedService.save(userExtendedDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userExtendedDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-extendeds : get all the userExtendeds.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userExtendeds in body
     */
    @GetMapping("/user-extendeds")
    public ResponseEntity<List<UserExtendedDTO>> getAllUserExtendeds(Pageable pageable) {
        log.debug("REST request to get a page of UserExtendeds");
        Page<UserExtendedDTO> page = userExtendedService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-extendeds");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /user-extendeds/:id : get the "id" userExtended.
     *
     * @param id the id of the userExtendedDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userExtendedDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-extendeds/{id}")
    public ResponseEntity<UserExtendedDTO> getUserExtended(@PathVariable Long id) {
        log.debug("REST request to get UserExtended : {}", id);
        Optional<UserExtendedDTO> userExtendedDTO = userExtendedService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userExtendedDTO);
    }

    /**
     * DELETE  /user-extendeds/:id : delete the "id" userExtended.
     *
     * @param id the id of the userExtendedDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-extendeds/{id}")
    public ResponseEntity<Void> deleteUserExtended(@PathVariable Long id) {
        log.debug("REST request to delete UserExtended : {}", id);
        userExtendedService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
