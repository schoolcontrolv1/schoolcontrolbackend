/**
 * View Models used by Spring MVC REST controllers.
 */
package com.school.control.web.rest.vm;
