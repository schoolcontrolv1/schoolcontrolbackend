package com.school.control.web.rest;
import com.school.control.service.UserDependentService;
import com.school.control.web.rest.errors.BadRequestAlertException;
import com.school.control.web.rest.util.HeaderUtil;
import com.school.control.web.rest.util.PaginationUtil;
import com.school.control.service.dto.UserDependentDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserDependent.
 */
@RestController
@RequestMapping("/api")
public class UserDependentResource {

    private final Logger log = LoggerFactory.getLogger(UserDependentResource.class);

    private static final String ENTITY_NAME = "userDependent";

    private final UserDependentService userDependentService;

    public UserDependentResource(UserDependentService userDependentService) {
        this.userDependentService = userDependentService;
    }

    /**
     * POST  /user-dependents : Create a new userDependent.
     *
     * @param userDependentDTO the userDependentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userDependentDTO, or with status 400 (Bad Request) if the userDependent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-dependents")
    public ResponseEntity<UserDependentDTO> createUserDependent(@RequestBody UserDependentDTO userDependentDTO) throws URISyntaxException {
        log.debug("REST request to save UserDependent : {}", userDependentDTO);
        if (userDependentDTO.getId() != null) {
            throw new BadRequestAlertException("A new userDependent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserDependentDTO result = userDependentService.save(userDependentDTO);
        return ResponseEntity.created(new URI("/api/user-dependents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-dependents : Updates an existing userDependent.
     *
     * @param userDependentDTO the userDependentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userDependentDTO,
     * or with status 400 (Bad Request) if the userDependentDTO is not valid,
     * or with status 500 (Internal Server Error) if the userDependentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-dependents")
    public ResponseEntity<UserDependentDTO> updateUserDependent(@RequestBody UserDependentDTO userDependentDTO) throws URISyntaxException {
        log.debug("REST request to update UserDependent : {}", userDependentDTO);
        if (userDependentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserDependentDTO result = userDependentService.save(userDependentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userDependentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-dependents : get all the userDependents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userDependents in body
     */
    @GetMapping("/user-dependents")
    public ResponseEntity<List<UserDependentDTO>> getAllUserDependents(Pageable pageable) {
        log.debug("REST request to get a page of UserDependents");
        Page<UserDependentDTO> page = userDependentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-dependents");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /user-dependents/:id : get the "id" userDependent.
     *
     * @param id the id of the userDependentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userDependentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-dependents/{id}")
    public ResponseEntity<UserDependentDTO> getUserDependent(@PathVariable Long id) {
        log.debug("REST request to get UserDependent : {}", id);
        Optional<UserDependentDTO> userDependentDTO = userDependentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userDependentDTO);
    }

    /**
     * DELETE  /user-dependents/:id : delete the "id" userDependent.
     *
     * @param id the id of the userDependentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-dependents/{id}")
    public ResponseEntity<Void> deleteUserDependent(@PathVariable Long id) {
        log.debug("REST request to delete UserDependent : {}", id);
        userDependentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
