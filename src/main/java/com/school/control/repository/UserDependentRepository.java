package com.school.control.repository;

import com.school.control.domain.UserDependent;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the UserDependent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserDependentRepository extends JpaRepository<UserDependent, Long> {

    @Query("select user_dependent from UserDependent user_dependent where user_dependent.parents.login = ?#{principal.username}")
    List<UserDependent> findByParentsIsCurrentUser();

    @Query("select user_dependent from UserDependent user_dependent where user_dependent.student.login = ?#{principal.username}")
    List<UserDependent> findByStudentIsCurrentUser();

}
