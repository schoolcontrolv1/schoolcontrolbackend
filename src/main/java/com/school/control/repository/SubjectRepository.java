package com.school.control.repository;

import com.school.control.domain.Subject;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Subject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {

    @Query("select subject from Subject subject where subject.student.login = ?#{principal.username}")
    List<Subject> findByStudentIsCurrentUser();

}
