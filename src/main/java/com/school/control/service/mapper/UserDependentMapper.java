package com.school.control.service.mapper;

import com.school.control.domain.*;
import com.school.control.service.dto.UserDependentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserDependent and its DTO UserDependentDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserDependentMapper extends EntityMapper<UserDependentDTO, UserDependent> {

    @Mapping(source = "parents.id", target = "parentsId")
    @Mapping(source = "student.id", target = "studentId")
    UserDependentDTO toDto(UserDependent userDependent);

    @Mapping(source = "parentsId", target = "parents")
    @Mapping(source = "studentId", target = "student")
    UserDependent toEntity(UserDependentDTO userDependentDTO);

    default UserDependent fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserDependent userDependent = new UserDependent();
        userDependent.setId(id);
        return userDependent;
    }
}
