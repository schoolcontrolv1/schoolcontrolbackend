package com.school.control.service.mapper;

import com.school.control.domain.*;
import com.school.control.service.dto.SubjectDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Subject and its DTO SubjectDTO.
 */
@Mapper(componentModel = "spring", uses = {QualificationMapper.class, UserMapper.class})
public interface SubjectMapper extends EntityMapper<SubjectDTO, Subject> {

    @Mapping(source = "qualification.id", target = "qualificationId")
    @Mapping(source = "student.id", target = "studentId")
    SubjectDTO toDto(Subject subject);

    @Mapping(source = "qualificationId", target = "qualification")
    @Mapping(source = "studentId", target = "student")
    Subject toEntity(SubjectDTO subjectDTO);

    default Subject fromId(Long id) {
        if (id == null) {
            return null;
        }
        Subject subject = new Subject();
        subject.setId(id);
        return subject;
    }
}
