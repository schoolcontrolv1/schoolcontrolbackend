package com.school.control.service.mapper;

import com.school.control.domain.*;
import com.school.control.service.dto.QualificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Qualification and its DTO QualificationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QualificationMapper extends EntityMapper<QualificationDTO, Qualification> {



    default Qualification fromId(Long id) {
        if (id == null) {
            return null;
        }
        Qualification qualification = new Qualification();
        qualification.setId(id);
        return qualification;
    }
}
