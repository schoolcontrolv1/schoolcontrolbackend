package com.school.control.service.mapper;

import com.school.control.domain.*;
import com.school.control.service.dto.UserExtendedDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserExtended and its DTO UserExtendedDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserExtendedMapper extends EntityMapper<UserExtendedDTO, UserExtended> {

    @Mapping(source = "user.id", target = "userId")
    UserExtendedDTO toDto(UserExtended userExtended);

    @Mapping(source = "userId", target = "user")
    UserExtended toEntity(UserExtendedDTO userExtendedDTO);

    default UserExtended fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserExtended userExtended = new UserExtended();
        userExtended.setId(id);
        return userExtended;
    }
}
