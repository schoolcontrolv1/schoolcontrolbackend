package com.school.control.service;

import com.school.control.domain.UserDependent;
import com.school.control.repository.UserDependentRepository;
import com.school.control.service.dto.UserDependentDTO;
import com.school.control.service.mapper.UserDependentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing UserDependent.
 */
@Service
@Transactional
public class UserDependentService {

    private final Logger log = LoggerFactory.getLogger(UserDependentService.class);

    private final UserDependentRepository userDependentRepository;

    private final UserDependentMapper userDependentMapper;

    public UserDependentService(UserDependentRepository userDependentRepository, UserDependentMapper userDependentMapper) {
        this.userDependentRepository = userDependentRepository;
        this.userDependentMapper = userDependentMapper;
    }

    /**
     * Save a userDependent.
     *
     * @param userDependentDTO the entity to save
     * @return the persisted entity
     */
    public UserDependentDTO save(UserDependentDTO userDependentDTO) {
        log.debug("Request to save UserDependent : {}", userDependentDTO);
        UserDependent userDependent = userDependentMapper.toEntity(userDependentDTO);
        userDependent = userDependentRepository.save(userDependent);
        return userDependentMapper.toDto(userDependent);
    }

    /**
     * Get all the userDependents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserDependentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserDependents");
        return userDependentRepository.findAll(pageable)
            .map(userDependentMapper::toDto);
    }


    /**
     * Get one userDependent by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<UserDependentDTO> findOne(Long id) {
        log.debug("Request to get UserDependent : {}", id);
        return userDependentRepository.findById(id)
            .map(userDependentMapper::toDto);
    }

    /**
     * Delete the userDependent by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserDependent : {}", id);        userDependentRepository.deleteById(id);
    }
}
