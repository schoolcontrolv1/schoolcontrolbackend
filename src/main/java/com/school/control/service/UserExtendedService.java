package com.school.control.service;

import com.school.control.domain.UserExtended;
import com.school.control.repository.UserExtendedRepository;
import com.school.control.service.dto.UserExtendedDTO;
import com.school.control.service.mapper.UserExtendedMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing UserExtended.
 */
@Service
@Transactional
public class UserExtendedService {

    private final Logger log = LoggerFactory.getLogger(UserExtendedService.class);

    private final UserExtendedRepository userExtendedRepository;

    private final UserExtendedMapper userExtendedMapper;

    public UserExtendedService(UserExtendedRepository userExtendedRepository, UserExtendedMapper userExtendedMapper) {
        this.userExtendedRepository = userExtendedRepository;
        this.userExtendedMapper = userExtendedMapper;
    }

    /**
     * Save a userExtended.
     *
     * @param userExtendedDTO the entity to save
     * @return the persisted entity
     */
    public UserExtendedDTO save(UserExtendedDTO userExtendedDTO) {
        log.debug("Request to save UserExtended : {}", userExtendedDTO);
        UserExtended userExtended = userExtendedMapper.toEntity(userExtendedDTO);
        userExtended = userExtendedRepository.save(userExtended);
        return userExtendedMapper.toDto(userExtended);
    }

    /**
     * Get all the userExtendeds.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserExtendedDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserExtendeds");
        return userExtendedRepository.findAll(pageable)
            .map(userExtendedMapper::toDto);
    }


    /**
     * Get one userExtended by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<UserExtendedDTO> findOne(Long id) {
        log.debug("Request to get UserExtended : {}", id);
        return userExtendedRepository.findById(id)
            .map(userExtendedMapper::toDto);
    }

    /**
     * Delete the userExtended by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserExtended : {}", id);        userExtendedRepository.deleteById(id);
    }
}
