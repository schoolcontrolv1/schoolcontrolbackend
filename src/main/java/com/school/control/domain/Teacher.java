package com.school.control.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import com.school.control.domain.enumeration.TeacherType;

import com.school.control.domain.enumeration.GradeLevels;

/**
 * A Teacher.
 */
@Entity
@Table(name = "teacher")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Teacher implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "number_iteam")
    private Integer numberIteam;

    @Column(name = "addres")
    private String addres;

    @Column(name = "category")
    private String category;

    @Enumerated(EnumType.STRING)
    @Column(name = "teacher_type")
    private TeacherType teacherType;

    @Enumerated(EnumType.STRING)
    @Column(name = "grade_levels")
    private GradeLevels gradeLevels;

    @Column(name = "activated")
    private Boolean activated;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberIteam() {
        return numberIteam;
    }

    public Teacher numberIteam(Integer numberIteam) {
        this.numberIteam = numberIteam;
        return this;
    }

    public void setNumberIteam(Integer numberIteam) {
        this.numberIteam = numberIteam;
    }

    public String getAddres() {
        return addres;
    }

    public Teacher addres(String addres) {
        this.addres = addres;
        return this;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public String getCategory() {
        return category;
    }

    public Teacher category(String category) {
        this.category = category;
        return this;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public TeacherType getTeacherType() {
        return teacherType;
    }

    public Teacher teacherType(TeacherType teacherType) {
        this.teacherType = teacherType;
        return this;
    }

    public void setTeacherType(TeacherType teacherType) {
        this.teacherType = teacherType;
    }

    public GradeLevels getGradeLevels() {
        return gradeLevels;
    }

    public Teacher gradeLevels(GradeLevels gradeLevels) {
        this.gradeLevels = gradeLevels;
        return this;
    }

    public void setGradeLevels(GradeLevels gradeLevels) {
        this.gradeLevels = gradeLevels;
    }

    public Boolean isActivated() {
        return activated;
    }

    public Teacher activated(Boolean activated) {
        this.activated = activated;
        return this;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Teacher createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public Teacher createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Teacher lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public Teacher lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Teacher teacher = (Teacher) o;
        if (teacher.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), teacher.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Teacher{" +
            "id=" + getId() +
            ", numberIteam=" + getNumberIteam() +
            ", addres='" + getAddres() + "'" +
            ", category='" + getCategory() + "'" +
            ", teacherType='" + getTeacherType() + "'" +
            ", gradeLevels='" + getGradeLevels() + "'" +
            ", activated='" + isActivated() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            "}";
    }
}
