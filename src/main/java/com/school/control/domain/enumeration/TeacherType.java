package com.school.control.domain.enumeration;

/**
 * The TeacherType enumeration.
 */
public enum TeacherType {
    ART, HISTORY, MUSIC, PHILOSOPHY, THEOLOGY, BIOLOFY, COMPUTER_SCIENCE, PHYSICS, PSYCHOLOGY, MATHEMATICS, PE, CHEMISTRY
}
