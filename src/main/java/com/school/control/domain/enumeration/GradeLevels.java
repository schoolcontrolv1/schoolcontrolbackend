package com.school.control.domain.enumeration;

/**
 * The GradeLevels enumeration.
 */
public enum GradeLevels {
    PRIMARY_LEVEL, MEDIUM_LEVEL, SECONDARY_LEVEL
}
